﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	public static PlayerManager instance;
	public PlayerUser currentPlayer;
    public PlayerCharacter currentCharacter;
	void Awake () {
		if(PlayerManager.instance != null){
			Destroy(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	public void SetPlayer(int _id, string _username, string _password){
		currentPlayer = new PlayerUser(_id,_username,_password);
		Debug.Log(currentPlayer.id + "," + currentPlayer.username + "," + currentPlayer.password);
	}

    public void SetCharacter(int _id, string _name, int _level, int _race_id, int _class_id, int _user_id, float _xPosition, float _yPosition) {
        currentCharacter = new PlayerCharacter(_id, _name, _level, _race_id, _class_id, _user_id, _xPosition, _yPosition);
    }
}

public class PlayerUser {
    public int id;
    public string username;
    public string password;

    public PlayerUser(int _id, string _username, string _password){
        id = _id;
        username = _username;
        password = _password;
    }
}

public class PlayerCharacter {
    public int id;
    public string name;
    public int level;
    public int race_id;
    public int class_id;
    public int player_id;
    public float xPosition;
    public float yPosition;

    public PlayerCharacter(int _id, string _name, int _level, int _race_id, int _class_id, int _player_id, float _xPosition, float _yPosition) {
        id = _id;
        name = _name;
        level = _level;
        race_id = _race_id;
        class_id = _class_id;
        player_id = _player_id;
        xPosition = _xPosition;
        yPosition = _yPosition;
    }
}
