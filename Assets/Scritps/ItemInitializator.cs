﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class ItemInitializator : NetworkBehaviour {

    public GameObject item;

    public override void OnStartServer() {
        ExecuteInitializeItems();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateItem(int characterID, int itemID, float posX, float posY) {
        Item tempItem = ItemManager.instance.itemDictionary[itemID];

        if (characterID != -1) {
            //Add item to the character
        } else {
            Vector3 tempPosition = new Vector3(posX, posY, 0);
            GameObject itemTempGameObject = Instantiate(item, tempPosition, Quaternion.identity);
            itemTempGameObject.GetComponent<ItManager>().Initialize(tempItem.fullName, itemID, false, characterID, posX, posY);
			NetworkServer.Spawn (itemTempGameObject);
        }
        
    }

    public void ExecuteInitializeItems() {
        StartCoroutine(InitializeItems());
    }

    public IEnumerator InitializeItems() {
        yield return new WaitForEndOfFrame();

		string conn = "URI=file:" + Application.persistentDataPath + "/DB/myDB.db"; //Path to database.
        Debug.Log(conn);
        IDbConnection dbconn;
        dbconn = (IDbConnection) new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.

        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = @"Select *
							From CharacterItem;";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        Debug.Log(reader.FieldCount);


        while (reader.Read()) {
            CreateItem(reader.GetInt32(0), reader.GetInt32(1), reader.GetFloat(2), reader.GetFloat(3));
        }

        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;

        dbconn.Close();
        dbconn = null;
    }
}
