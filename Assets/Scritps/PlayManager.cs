﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;


using System;
using System.Data;
using Mono.Data.Sqlite;

public class PlayManager : NetworkBehaviour {

    public SpriteRenderer character;
    public SpriteRenderer weapon;

    public GameObject currentItem;

    public Sprite warrior;
    public Sprite rogue;
    public Sprite mage;

    public Sprite human;
    public Sprite dwarf;
    public Sprite elf;

    public float dropTimer;
    float dropStart;

    public float timeToSavePos;
    float savePosStart;

    public Text userInfo;

    void Start() {
        if (isLocalPlayer) {
            userInfo = GameObject.Find("userInfo").GetComponent<Text>();
            LoadCharacter();
            savePosStart = Time.time;
        }
    }

    void Update() {
        if (isLocalPlayer) {
            if (Time.time - savePosStart >= timeToSavePos) {
                Debug.Log("Saving CharacterPosition");
                //ExecuteSaveCharacterPosition();
                savePosStart = Time.time;
            }
        }
    }

    public void LoadThisScene(int scene) {
        SceneManager.LoadScene(scene);
    }

    public void LoadCharacter() {


        switch (PlayerManager.instance.currentCharacter.race_id) {
            default:
                break;
            case 1:
                character.sprite = human;
                break;
            case 2:
                character.sprite = dwarf;
                break;
            case 3:
                character.sprite = elf;
                break;
        }
        switch (PlayerManager.instance.currentCharacter.class_id) {
            default:
                break;
            case 1:
                weapon.sprite = warrior;
                break;
            case 2:
                weapon.sprite = rogue;
                break;
            case 3:
                weapon.sprite = mage;
                break;
        }
        PlaceInWorld(PlayerManager.instance.currentCharacter.xPosition, PlayerManager.instance.currentCharacter.yPosition);
        UpdateText();
    }

    public void PlaceInWorld(float xPos, float yPos) {
        character.gameObject.transform.position = new Vector2(xPos, yPos);
    }

    public void UpdateText() {
        userInfo.text = PlayerManager.instance.currentCharacter.name + " Level: " + PlayerManager.instance.currentCharacter.level;
    }

    public void AddItem(GameObject item) {
        //DeEquip object
        if (currentItem != null) {
            DropItem(item);
        }
        //Equip object
        item.GetComponent<ItManager>().isInPlayer = true;
        item.GetComponent<ItManager>().playerId = 1;//This should be the playerID
        item.transform.parent = transform;
        item.transform.position = transform.position;
        currentItem = item;
        //Start Timer
        dropStart = Time.time;
		//GetComponent<Mov>().CmdFireCross ();
    }

    public void DropItem() {
        if (currentItem != null) {
            if (Time.time - dropStart >= dropTimer) {
                currentItem.GetComponent<ItManager>().isInPlayer = false;
                currentItem.GetComponent<ItManager>().playerId = -1;
                currentItem.GetComponent<ItManager>().UpdatePosition();
                currentItem.GetComponent<ItManager>().ExecuteSaveItemPosition();
                currentItem.transform.parent = null;
                currentItem = null;
				//GetComponent<Mov>().CmdFireEx ();
            }
        }
    }

    public void DropItem(GameObject item) {
        if (Time.time - dropStart >= dropTimer) {
            currentItem.GetComponent<ItManager>().isInPlayer = false;
            currentItem.GetComponent<ItManager>().playerId = -1;
            currentItem.transform.parent = null;
            currentItem.transform.position = item.transform.position;
            currentItem.GetComponent<ItManager>().UpdatePosition();
            currentItem.GetComponent<ItManager>().ExecuteSaveItemPosition();
            currentItem = null;
			//GetComponent<Mov>().CmdFireEx ();
        }
    }

    public void ExecuteChangeCharacterLvl(int ammount) {
        StartCoroutine(ChangeCharacterLvl(ammount));
    }

    public IEnumerator ChangeCharacterLvl(int ammount) {
        yield return new WaitForEndOfFrame();

		string conn = "URI=file:" + Application.persistentDataPath + "/DB/myDB.db"; //Path to database.

        Debug.Log(conn);
        IDbConnection dbconn;
        dbconn = (IDbConnection) new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.

        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = @"UPDATE PlayerCharacters
                            SET level = level + " + ammount + @"
                            WHERE name = '" + PlayerManager.instance.currentCharacter.name + "';";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        PlayerManager.instance.currentCharacter.level += ammount;

        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;

        dbconn.Close();
        dbconn = null;

        UpdateText();
    }

/*    public void ExecuteSaveCharacterPosition() {
        StartCoroutine(SaveCharacterPosition());
    }

    public IEnumerator SaveCharacterPosition() {
        yield return new WaitForEndOfFrame();

		string conn = "URI=file:" + Application.persistentDataPath + "/DB/myDB.db"; //Path to database.
        Debug.Log(conn);
        IDbConnection dbconn;
        dbconn = (IDbConnection) new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.

        IDbCommand dbcmd = dbconn.CreateCommand();

        string sqlQuery = @"UPDATE PlayerCharacters
                            SET worldPosX = " + character.gameObject.transform.position.x + ", worldPosY = " + character.gameObject.transform.position.y + @"
                            WHERE name = '" + PlayerManager.instance.currentCharacter.name + "';";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;

        dbconn.Close();
        dbconn = null;

        UpdateText();
    }*/
}
