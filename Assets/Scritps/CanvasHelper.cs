﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHelper : MonoBehaviour {

    public static CanvasHelper instance;

    public GameObject emptyError;
    public GameObject falseError;

    void Start() {
        instance = this;
    }

    public void DeactivateObject(GameObject obj) {
        obj.SetActive(false);
    }

    public void EmptyError() {
        emptyError.SetActive(true);
    }

    public void FalseError() {
        falseError.SetActive(true);
    }
}
