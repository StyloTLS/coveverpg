﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class CreateCharacterManager : Connect {

	public InputField characterName;
	public Dropdown race;
	public Dropdown rpgClass;

	public void LoadThisScene(int scene){
		SceneManager.LoadScene(scene);
	}

	public void ExecuteCreateCharacter(){
		if(characterName.text != ""){
            StartCoroutine(CreateCharacter());
        } else {
            CanvasHelper.instance.EmptyError();
        }
	}

	public IEnumerator CreateCharacter() {
        yield return new WaitForEndOfFrame();

        try {
			Access();
            dbcmd = dbconn.CreateCommand();
            int theRace = -1;
            int theClass = -1;

            switch (race.value) {
                case 0:
                    theRace = 1;
                    break;
                case 1:
                    theRace = 2;
                    break;
                case 2:
                    theRace = 3;
                    break;
                default:
                    break;
            }
            switch (rpgClass.value) {
                case 0:
                    theClass = 1;
                    break;
                case 1:
                    theClass = 2;
                    break;
                case 2:
                    theClass = 3;
                    break;
                default:
                    break;
            }
            string sqlQuery = @"INSERT INTO PlayerCharacters(name,level,race_id,class_id,user_id,worldPosX,worldPosY) VALUES ('" + characterName.text + "',1," + theRace + "," + theClass + "," + PlayerManager.instance.currentPlayer.id + ",0,0);";
            dbcmd.CommandText = sqlQuery;
            reader = dbcmd.ExecuteReader();
            CleanUp();
            SceneManager.LoadScene(1);

        } catch (Exception) {
            CanvasHelper.instance.FalseError();
        }
    }
}
