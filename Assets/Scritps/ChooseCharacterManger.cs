﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class ChooseCharacterManger : Connect {

	public List<CharacterSlot> slots;

    public GameObject pleaseWait;

	void Start(){
		ExecuteLoadCharacters ();
	}

	public void ExecuteLoadCharacters(){
		StartCoroutine(LoadCharacters());
	}

    public void ExecuteDeleteCharacter(CharacterSlot slot) {
        pleaseWait.SetActive(true);
        StartCoroutine(DeleteCharacter(slot));
    }

	public IEnumerator LoadCharacters(){
		yield return new WaitForEndOfFrame();

		int currentSlot = 0;

		Access();

        dbcmd = dbconn.CreateCommand();
        
        string sqlQuery = @"Select *
							From PlayerCharacters
							Where PlayerCharacters.user_id = " + PlayerManager.instance.currentPlayer.id + ";";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

		while(reader.Read()){
			slots[currentSlot].SetSlot(reader.GetInt32(0), reader.GetString(1),reader.GetInt32(2),reader.GetInt32(3),reader.GetInt32(4), reader.GetInt32(5), reader.GetFloat(6), reader.GetFloat(7));
			currentSlot++;
		}

        CleanUp();

        pleaseWait.SetActive(false);
	}

    public IEnumerator DeleteCharacter(CharacterSlot slot) {
        yield return new WaitForEndOfFrame();

		Access();

        dbcmd = dbconn.CreateCommand();

        string sqlQuery = @"UPDATE PlayerCharacters
                            SET user_id = -1
                            WHERE name = '" + slot.characterName + "';";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        CleanUp();

        GoToScene(1);
    }

    public void GoToScene(int scene){
		SceneManager.LoadScene(scene);
	}
}
