﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class ItemManager : Connect {

    public static ItemManager instance;
    public Dictionary<int, Item> itemDictionary = new Dictionary<int, Item>();

    void Awake() {
        if (ItemManager.instance != null) {
            Destroy(gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(gameObject);
            ExecuteLoadItems();
        }
    }

    public void ExecuteLoadItems() {
        StartCoroutine(LoadItems());
    }
    
    public IEnumerator LoadItems() {
        yield return new WaitForEndOfFrame();
        
		Access();
        dbcmd = dbconn.CreateCommand();

        string sqlQuery = @"Select *
							From Items;";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        Debug.Log(reader.FieldCount);
        

        while (reader.Read()) {
            Item tempItem = new Item(reader.GetString(2), reader.GetString(1), reader.GetString(3));
            itemDictionary.Add(reader.GetInt32(0), tempItem);
        }

        CleanUp();
    }
}

public class Item {
    public string pref;
    public string name;
    public string suf;

    public string fullName;

    public Item() {
        pref = null;
        name = null;
        suf = null;

        fullName = null;
    }

    public Item(string _name, string _pref = null, string _suf = null) {
        pref = _pref;
        name = _name;
        suf = _suf;

        fullName = pref + " " +  name + " " + suf;
    }
}
