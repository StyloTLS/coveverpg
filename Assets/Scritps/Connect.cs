﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class Connect : MonoBehaviour {

	protected IDbConnection dbconn;
	protected IDataReader reader = null;
	protected IDbCommand dbcmd = null;
	protected void Access(){
		string conn = "URI=file:" + Application.persistentDataPath + "/DB/myDB.db";
		Debug.Log(conn);
        dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open();
	}

	protected void CleanUp(){
		reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

	}


}
