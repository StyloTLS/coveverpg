﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class Connection : Connect {

    public InputField user;
    public InputField pass;

    public void ExecuteLogIn(){
        if(user.text != "" && pass.text != ""){
            StartCoroutine(LogIn());
        } else {
            CanvasHelper.instance.EmptyError();
        }
    }

    public void ExecuteSignUp(){
        if(user.text != "" && pass.text != ""){
            StartCoroutine(SignUp());
        } else {
            CanvasHelper.instance.EmptyError();
        }
    }

    public IEnumerator LogIn() {
        yield return new WaitForEndOfFrame();

        bool canLoad = false;

		Access();
        dbcmd = dbconn.CreateCommand();
        string sqlQuery = @"Select *
                            From Users
                            Where Users.username = '" + user.text + @"'
                            And Users.pass = '" + pass.text + "';";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        while (reader.Read()) {
            if (reader.GetFloat(0) > 0) {
                canLoad = true;
                PlayerManager.instance.SetPlayer(reader.GetInt32(0), reader.GetString(1), reader.GetString(2));
            }
        }

        CleanUp();

        if (canLoad) {
            SceneManager.LoadScene(1);
        } else {
            CanvasHelper.instance.FalseError();
        }
    }

    public IEnumerator SignUp() {
        yield return new WaitForEndOfFrame();

        Access();
        dbcmd = dbconn.CreateCommand();
        string sqlQuery = @"INSERT INTO Users(id, username, pass)
                            VALUES(NULL, '" + user.text + "', '" + pass.text + "');";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();        
        CleanUp();        
        Debug.Log("Usuario Creado");
    }

    void Connect () {
        Access();
		dbcmd = dbconn.CreateCommand();
		string sqlQuery = "SELECT * FROM users;";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();
		
		while (reader.Read())
		{
			string username = reader.GetString(1);
			string passworld = reader.GetString(2);


            Debug.Log("username= " + username + "  password =" + passworld);
        }        
        CleanUp();
	}
	
}
