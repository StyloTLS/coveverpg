﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System;
using System.Data;
using Mono.Data.Sqlite;

public class ItManager : Connect {

    public string itemName;
    public int itemId;

    public bool isInPlayer;
    public int playerId;

    public float currentX;
    public float currentY;

    public void Initialize(string _itemName, int _itemId, bool _isInPlayer, int _playerId, float _currentX, float _currentY) {
        itemName = _itemName;
        itemId = _itemId;

        isInPlayer = _isInPlayer;
        playerId = _playerId;

        transform.localPosition = new Vector3(_currentX,_currentY);
        currentX = transform.position.x;
        currentY = transform.position.y;
    }

    public void UpdatePosition() {
        currentX = transform.position.x;
        currentY = transform.position.y;
        currentX = (float) Math.Round(currentX, 2);
        currentY = (float) Math.Round(currentY, 2);
    }

    public void ExecuteSaveItemPosition() {
        StartCoroutine(SaveItemPosition());
    }

    public IEnumerator SaveItemPosition() {
        yield return new WaitForEndOfFrame();

		Access();
        dbcmd = dbconn.CreateCommand();

                string sqlQuery = @"UPDATE CharacterItem
                                    SET Character_id = " + playerId + ", worldPosX = " + currentX + ", worldPosY = " + currentY + @"
                                    Where Item_id = " + itemId + ";";

        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        CleanUp();
    }
}
