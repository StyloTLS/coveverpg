﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Races : MonoBehaviour {
static public Races instance;

	public Sprite human;
	public Sprite dwarf;
	public Sprite elf;

	void Start() {
		instance = this;
	}

	public Sprite GetSprite(string spriteName){
		Sprite sprite = null;

		switch (spriteName.ToLower()) {
        
        case "human": 
        sprite = human;
        break;
        case "dwarf": 
        sprite = dwarf;
        break;
        case "elf": 
        sprite = elf;
        break;
        }

		return sprite;
	}

}
