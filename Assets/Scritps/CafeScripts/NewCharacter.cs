﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewCharacter : MonoBehaviour {

	[SerializeField]
	private string raceSelected;

	private SpriteRenderer spriteRenderer;

	public Dropdown dropRace;


	// Use this for initialization
	void Start () {
	spriteRenderer= GetComponent<SpriteRenderer>();


	}
	
	// Update is called once per frame
	void Update () {
		raceSelected = dropRace.options[dropRace.value].text;
		ChangeRace ();
	}

	void ChangeRace() {
		spriteRenderer.sprite=Races.instance.GetSprite(raceSelected);
	
	}
}
