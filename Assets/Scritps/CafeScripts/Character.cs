﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character {

	public int id;
	public int userId;
	public string name;
	public int race;
	public int className;
	public int level;

}
