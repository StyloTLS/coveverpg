﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewClass : MonoBehaviour {


	[SerializeField]
	private string classSelected;

	private SpriteRenderer spriteRenderer;

	public Dropdown dropClass;


	// Use this for initialization
	void Start () {
spriteRenderer= GetComponent<SpriteRenderer>();


	}
	
	// Update is called once per frame
	void Update () {
		classSelected = dropClass.options [dropClass.value].text;
		ChangeClass ();

	}

	void ChangeClass() {
			spriteRenderer.sprite=ClassName.instance.GetClassSprite(classSelected);
	}
}
