﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.Networking;

public class Mov : NetworkBehaviour {

	public GameObject azucarPrefab;
	public GameObject detectedItem;

	public List<Transform> crossSpawn;
	public List<Transform> exSpawn;

	void Start () {
		
	}

	void Update () {
        if (!isLocalPlayer) {
            return;
        }
		if (Input.GetKey(KeyCode.W)) {
			gameObject.transform.Translate (Vector3.up*Time.deltaTime*2);
		}
		if (Input.GetKey(KeyCode.S)) {
			gameObject.transform.Translate (Vector3.down*Time.deltaTime*2);
		}
		if (Input.GetKey(KeyCode.A)) {
			gameObject.transform.Translate (Vector3.left*Time.deltaTime*2);
		}
		if (Input.GetKey(KeyCode.D)) {
			gameObject.transform.Translate (Vector3.right*Time.deltaTime*2);
		}
        if (Input.GetKeyDown(KeyCode.Space)) {
			if(GetComponent<PlayManager>().currentItem != null){
				GetComponent<PlayManager>().DropItem();
			}
        }
    }

	void OnTriggerEnter2D(Collider2D collision){
		if(isLocalPlayer){
			if (collision.CompareTag("Item") && !(collision.GetComponent<ItManager>().isInPlayer)) {
				detectedItem = collision.gameObject;
			}
		}
	}

    void OnTriggerStay2D(Collider2D collision) {
        if (isLocalPlayer) {
            if (collision.CompareTag("Item") && !(collision.GetComponent<ItManager>().isInPlayer)) {
                if (Input.GetKeyDown(KeyCode.Space)) {
                    //Agregar al inventario
					if (detectedItem != null) {
						GetComponent<PlayManager>().AddItem(detectedItem);
					} else {
						Debug.LogError ("How can you pick up something that doesn[apostrofe]t exist");
					}
                }
            }
        }
    }

	[Command]
	public void CmdFireCross() {
		try {
			Debug.Log("Aqui we");
			for (int i = 0; i < crossSpawn.Count; i++) {
				GameObject azucar = Instantiate(azucarPrefab, crossSpawn[i].position, crossSpawn[i].rotation);
				azucar.GetComponent<Rigidbody2D>().velocity = azucar.transform.up * 6f;
				azucar.GetComponent<Azucar>().father = gameObject;
				NetworkServer.Spawn(azucar);
				Destroy(azucar, 6f);
			}
		} catch (System.Exception ex) {
			Debug.LogError ("Wow you failed");
		}
	}

	public void CmdFireEx() {
		try {
			Debug.Log("Aqui no we");
			for (int i = 0; i < crossSpawn.Count; i++) {
				GameObject azucar = Instantiate(azucarPrefab, exSpawn[i].position, exSpawn[i].rotation);
				azucar.GetComponent<Rigidbody2D>().velocity = azucar.transform.up * 6f;
				azucar.GetComponent<Azucar>().father = gameObject;
				NetworkServer.Spawn(azucar);
				Destroy(azucar, 6f);
			}
		} catch (System.Exception ex) {
			Debug.LogError ("Wow you failed");
		}
	}
}
