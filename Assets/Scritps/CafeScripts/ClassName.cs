﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassName:MonoBehaviour {

static public ClassName instance;

	public Sprite warrior;
	public Sprite rogue;
	public Sprite mage;

	void Start() {
		instance = this;
	}

	public Sprite GetClassSprite(string spriteName){
		Sprite sprite = null;

		switch (spriteName.ToLower()) {
        
        case "warrior": 
        sprite = warrior;
        break;
        case "rogue": 
        sprite = rogue;
        break;
        case "mage": 
        sprite = mage;
        break;
        }

		return sprite;
	}
}
