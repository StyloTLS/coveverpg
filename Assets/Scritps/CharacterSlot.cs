﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSlot : MonoBehaviour {

    public ChooseCharacterManger chooseCharacterManager;

	public GameObject used;
	public GameObject unused;

	public int id;
	public string characterName;
	public int level;
	public int race_id;
	public int class_id;
	public int user_id;
    public float xPosition;
    public float yPosition;

	public Text nameDisplay;
	public Text levelDisplay;
	public Text raceDisplay;
	public Text classDisplay;

    public void DeleteThisSlot() {
        chooseCharacterManager.ExecuteDeleteCharacter(this);
    }

    public void ChooseSlot() {
        PlayerManager.instance.SetCharacter(id, characterName, level, race_id, class_id, user_id, xPosition, yPosition);
        SceneManager.LoadScene(3);
    }

    public void SetSlot(int _id, string _name, int _level, int _race_id, int _class_id, int _user_id, float _xPosition, float _yPosition) {
		nameDisplay.text = _name;
		levelDisplay.text = "Level " + _level.ToString();
		switch(_race_id){
			case 1:
				raceDisplay.text = "Human";
				break;
			case 2:
				raceDisplay.text = "Dwarf";
				break;
			case 3:
				raceDisplay.text = "Elf";
				break;
			default:
				Debug.LogError("That race doesn't exist");
				break;
		}
		switch(_class_id){
			case 1:
				classDisplay.text = "Warrior";
				break;
			case 2:
				classDisplay.text = "Rogue";
				break;
			case 3:
				classDisplay.text = "Mage";
				break;
			default:
				Debug.LogError("That race doesn't exist");
				break;
		}
        id = _id;
        characterName = _name;
        level = _level;
        race_id = _race_id;
        class_id = _class_id;
        user_id = _user_id;
        xPosition = _xPosition;
        yPosition = _yPosition;
		IsActive(true);
	}

	public void IsActive(bool active){
		used.SetActive(active);
		unused.SetActive(!active);
	}
}
